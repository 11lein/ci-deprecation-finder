# Find CI deprecations

Parse CI files of all projects and [identify 16.0 breaking change deprecations](https://docs.gitlab.com/ee/update/deprecations.html?breaking_only=true&removal_milestone=160).

## Usage

### Report format

The report contains the lift of all projects with their paths on URLs, as well as columns for each deprecation. `TRUE` marks that the project is affected by the deprecation and should be adjusted before 16.0.

### Forking and running this project on GitLab.com

If you use GitLab SaaS, you can simply:
* fork this project
* edit `config.yml` to define groups and projects
* add your access token as a CI/CD Variable `$GIT_TOKEN`
  * The access token needs `api` and `read_repository` rights
* run a pipeline
* open the `create_report` job and get the `ci_deprecation_report.csv` from the artifacts

### Installing locally or on self-hosted GitLab

`pip3 install -r requirements.txt`

### Running the report

`python3 find_ci_criteria.py $GIT_TOKEN $CONFIG_YML --gitlab $GITLAB_URL`

* $GIT_TOKEN: An access token with API and Repository permissions for the configured groups and projects
* $CONFIG_YML: Path to the `config.yml` file containing the configuration
* $GITLAB_URL: Optional URL of the GitLab instance. Defaults to `https://gitlab.com`

## Configuration

### Define groups or projects

- edit config.yml
  - specify `groups` or `projects` as arrays using their ID
  - If groups are used, all projects in the group are retrieved instead of individual projects.
  - If neither are defined, all groups the token has access to will be used to retrieve projects. On self-managed with an admin token, that's equivalent to all projects on the instance

## Included deprecations

| Deprecation                                                                                                                                                                                   | Issue                                                | Search CI path                                                                    |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------|-----------------------------------------------------------------------------------|
| [CI_JOB_JWT 1/2 deprecated](https://docs.gitlab.com/ee/update/deprecations.html#old-versions-of-json-web-tokens-are-deprecated)                                                               | https://gitlab.com/gitlab-org/gitlab/-/issues/366798 | CI_JOB_JWT\|CI_JOB_JWT_V2                                                         |
| [Vault integration requires id_tokens with aud](https://docs.gitlab.com/ee/update/deprecations.html#hashicorp-vault-integration-will-no-longer-use-ci_job_jwt-by-default)                     | https://gitlab.com/gitlab-org/gitlab/-/issues/366798 | secrets.*vault lacking secrets.*token or id_tokens.*aud                           |
| [CI keyword 255 character limit](https://docs.gitlab.com/ee/update/deprecations.html#enforced-validation-of-cicd-parameter-character-lengths)                                                 | https://gitlab.com/gitlab-org/gitlab/-/issues/372770 | stage:.{256,}$\|ref:.{256,}$\|target_url:.{256,}$\|description:.{256,}$           |
| [CI_BUILD_* variables replaced](https://docs.gitlab.com/ee/update/deprecations.html#ci_build_-predefined-variables)                                                                           | https://gitlab.com/gitlab-org/gitlab/-/issues/352957 | CI_BUILD_.*                                                                       |
| [DAST API variables changed](https://docs.gitlab.com/ee/update/deprecations.html#dast-api-variables)                                                                                          | https://gitlab.com/gitlab-org/gitlab/-/issues/383467 | DAST_API_HOST_OVERRIDE\|DAST_API_SPECIFICATION                                    |
| [DAST ZAP advanced configuration variables deprecation](https://docs.gitlab.com/ee/update/deprecations.html#dast-zap-advanced-configuration-variables-deprecation)                            | https://gitlab.com/gitlab-org/gitlab/-/issues/383467 | DAST_ZAP_CLI_OPTIONS\|DAST_ZAP_LOG_CONFIGURATION                                  |
| [DAST report variables deprecation](https://docs.gitlab.com/ee/update/deprecations.html#dast-report-variables-deprecation)                                                                    | https://gitlab.com/gitlab-org/gitlab/-/issues/384340 | DAST_HTML_REPORT\|DAST_XML_REPORT\|DAST_MARKDOWN_REPORT                           |
| [License Compliance CI Template deprecated](https://docs.gitlab.com/ee/update/deprecations.html#license-compliance-ci-template)                                                               | https://gitlab.com/gitlab-org/gitlab/-/issues/387561 | license_scanning:image:name:.*license-finder                                      |
| [Container Scanning variables that reference Docker](https://docs.gitlab.com/ee/update/deprecations.html#container-scanning-variables-that-reference-docker)                                  | https://gitlab.com/gitlab-org/gitlab/-/issues/371840 | container_scanning.*(DOCKER_IMAGE\|DOCKER_PASSWORD\|DOCKER_USER\|DOCKERFILE_PATH) |
| [CI_PRE_CLONE_SCRIPT variable on GitLab SaaS deprecated](https://docs.gitlab.com/ee/update/deprecations.html#deprecation-and-planned-removal-for-ci_pre_clone_script-variable-on-gitlab-saas) | https://gitlab.com/gitlab-org/gitlab/-/issues/391896 | CI_PRE_CLONE_SCRIPT                                                               |

## DISCLAIMER

This script is provided **for educational purposes only**. It is not supported by GitLab. You can create an issue or contribute via MR if you encounter any problems.
